package chapter6.service;

import static chapter6.utils.DBUtil.*;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.utils.DBUtil;

// 1. テストクラス名は任意のものに変更してください。
// 2. L.23~86は雛形として使用してください。
// 3．L.44のファイル名は各自作成したファイル名に書き換えてください。
public class MessageServiceTest {

	private File file;

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSetMessages = new FlatXmlDataSet(new File("messages_test_data.xml"));
			IDataSet dataSetUsers = new FlatXmlDataSet(new File("users_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetMessages);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetUsers);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	// 以下に課題のテストメソッドを作成

	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testSelect() throws Exception {

		List<UserMessage> resultList = new MessageService().select("1", null, null);

		assertEquals(2, resultList.size());

		UserMessage result001 = resultList.get(0);
		assertEquals("text=1人目のつぶやき", "text=" + result001.getText());
		assertEquals("userId=1", "userId=" + result001.getUserId());
		assertEquals("account=アカウント1", "account=" + result001.getAccount());
		assertEquals("name=ひとりめ", "name=" + result001.getName());

		UserMessage result002 = resultList.get(1);
		assertEquals("userId=1", "userId=" +  result002.getUserId());
		assertEquals("text=1人目の2回目のつぶやき", "text=" + result002.getText());
		assertEquals("account=アカウント1", "account=" + result001.getAccount());
		assertEquals("name=ひとりめ", "name=" + result001.getName());
		}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testInsert() throws Exception {

		Message message001 = new Message();
		message001.setUserId(1);
		message001.setText("テストつぶやき");

		new MessageService().insert(message001);

		IDatabaseConnection connection = null;
		try {
		Connection conn = getConnection();
		connection = new DatabaseConnection(conn);

		//メソッド実行した実際のテーブル
		IDataSet databaseDataSet = connection.createDataSet();
		ITable actualTable = databaseDataSet.getTable("messages");

		// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDataSet expectedDataSet = new FlatXmlDataSet(new File("messages_test_data2.xml"));
		ITable expectedTable = expectedDataSet.getTable("messages");

		ITable filteredTable
	      = DefaultColumnFilter.includedColumnsTable(actualTable,
	                                                 expectedTable.getTableMetaData().getColumns());

		//期待されるITableと実際のITableの比較
		Assertion.assertEquals(expectedTable, filteredTable);

		} finally {
		if (connection != null)
		connection.close();
		}
	}
}