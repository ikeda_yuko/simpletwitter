package chapter6.service;

import static chapter6.utils.DBUtil.*;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.User;
import chapter6.utils.DBUtil;

// 1. テストクラス名は任意のものに変更してください。
// 2. L.23~86は雛形として使用してください。
// 3．L.44のファイル名は各自作成したファイル名に書き換えてください。
public class UserServiceTest {

	private File file;

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSetUsers = new FlatXmlDataSet(new File("users_test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetUsers);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}
	}

	// 以下に課題のテストメソッドを作成
	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testSelect() throws Exception {

		String account = "アカウント1";
		String password = "password1";

		User result = new UserService().select(account, password);

		assertEquals("account=アカウント1", "account=" + result.getAccount());
		assertEquals("name=ひとりめ", "name=" + result.getName());
		assertEquals("email=abc@ne.jp", "email=" + result.getEmail());
		assertEquals("password=CxTVAaWURCoBxoWVQbyz6BZNGD0yk3uFGDVEL2nVyU4", "password=" + result.getPassword());
		}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testInsert() throws Exception {

		User user001 = new User();
		user001.setAccount("テストアカウント");
		user001.setName("テスト名前");
		user001.setEmail("test@ne.jp");
		user001.setPassword("testPassword");
		user001.setDescription("");

		new UserService().insert(user001);

		IDatabaseConnection connection = null;
		try {
		Connection conn = getConnection();
		connection = new DatabaseConnection(conn);

		//メソッド実行した実際のテーブル
		IDataSet databaseDataSet = connection.createDataSet();
		ITable actualTable = databaseDataSet.getTable("users");

		// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDataSet expectedDataSet = new FlatXmlDataSet(new File("users_test_data2.xml"));
		ITable expectedTable = expectedDataSet.getTable("users");

		ITable filteredTable
	      = DefaultColumnFilter.includedColumnsTable(actualTable,
	                                                 expectedTable.getTableMetaData().getColumns());

		//期待されるITableと実際のITableの比較
		Assertion.assertEquals(expectedTable, filteredTable);

		} finally {
		if (connection != null)
		connection.close();
		}
	}

	@Test
	public void testUpdate() throws Exception {

		User user001 = new User();
		user001.setId(1);
		user001.setAccount("更新テストアカウント");
		user001.setName("更新テスト名前");
		user001.setEmail("updateTest@ne.jp");
		user001.setPassword("update");
		user001.setDescription("");

		new UserService().update(user001);

		IDatabaseConnection connection = null;
		try {
		Connection conn = getConnection();
		connection = new DatabaseConnection(conn);

		//メソッド実行した実際のテーブル
		IDataSet databaseDataSet = connection.createDataSet();
		ITable actualTable = databaseDataSet.getTable("users");

		// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
		IDataSet expectedDataSet = new FlatXmlDataSet(new File("users_test_data3.xml"));
		ITable expectedTable = expectedDataSet.getTable("users");

		ITable filteredTable
	      = DefaultColumnFilter.includedColumnsTable(actualTable,
	                                                 expectedTable.getTableMetaData().getColumns());

		//期待されるITableと実際のITableの比較
		Assertion.assertEquals(expectedTable, filteredTable);

		} finally {
		if (connection != null)
		connection.close();
		}
	}

}
